var express = require('express');
var router = express.Router();
var Instagram = require('instagram-node').instagram();
var Twitter = require('twitter');
var database = require('../lib/database')();
var hashtag = "happilyEverApperley";
var moment = require('moment');

var twitterStream = null;
var twitter_client = new Twitter({
    consumer_key: 'ra0yo2rQjBGnEsyMj0p8rOsbI',
    consumer_secret: 'xgIGbb3kPXxd5y9SqVnaRmlSHVHwgEzH3hIg2TD7A11zuJ6XgT',
    access_token_key: '88963301-WXWZ7j9PYjXlwsf2THbxVvCZCuzP8rw98MDlCAc7x',
    access_token_secret: '2ugkZr0Xz7qYt5gPdkc8ywsH3xO06k9MPCzJzwLu65wXt'
});

Instagram.use({ client_id: '51789d73802e426f826eea31d8658c29',
    client_secret: 'da64edf5cf2940cf8176841f75f1204a' });

function insertMedia(models, callback) {
    database.pool().getConnection(function(err, connection) {
        if(err) {
            console.log("Connection creation error ", err);
        }
        var query = "";
        for (var i = 0; i < models.length; i++) {
            query += "INSERT INTO happilyeverapperley.Social SET ?;";
        }
        connection.query(query, models, function(err, result) {
            if(err) {
                console.log("Social Media INSERT error ", err);
            }
            connection.release();
            if (callback) {
                callback();
            }
        });
    });
}

// Start and Stop the services
router.get('/start', function(req, res) {
    startTwitter();
    startInstagram();
    res.send("started");
});

function startTwitter() {
    if (!twitterStream) {
        twitter_client.stream('statuses/filter', {track: "#"+hashtag}, function(stream) {
            twitterStream = stream;
            stream.on('data', function(tweet) {
                var data = {
                    id: tweet.id_str,
                    text: tweet.text,
                    name: tweet.user.name,
                    type: 'twitter',
                    screen_name: tweet.user.screen_name,
                    hashtags: JSON.stringify(tweet.entities.hashtags),
                    media: (tweet.entities.media !== undefined) ? tweet.entities.media[0].media_url : "",
                    timestamp: moment(tweet.created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').unix()
                };
                insertMedia([data], null);
            });

            stream.on('error', function(error) {
                throw error;
            });
        });
    }
}

function stopTwitter() {
    if (twitterStream) {
        twitterStream.destroy();
    }
}

var instagramTimer = null;
var lastId = null;
var instagramCallback = function(err, medias, pagination, remaining, limit) {

    if (!instagramTimer) {return;}

    var grams = [];
    for (var i = 0; i < medias.length; i++) {
        var media = "";
        if (medias[i].type == "image") {
            media = medias[i].images.standard_resolution.url;
        } else {
            media = medias[i].videos.standard_resolution.url;
        }
        grams.push({
            id: medias[i].id,
            text: (medias[i].caption) ? medias[i].caption.text : "",
            name: medias[i].user.full_name,
            type: 'instagram',
            screen_name: medias[i].user.username,
            hashtags: JSON.stringify(medias[i].tags),
            media: media,
            timestamp: medias[i].created_time
        });
    }
    insertMedia(grams, function() {
        if(pagination.next) {
            pagination.next(instagramCallback);
        } else {
            lastId = medias[medias.length-1].id;
        }
    });
};

function startInstagram() {
    if(!instagramTimer) {
        function run() {
            Instagram.tag_media_recent(hashtag, (lastId) ? {MAX_TAG_ID: lastId} : {}, instagramCallback);
        }
        instagramTimer = setInterval(run, 300000);
        run();
    }
}

function stopInstagram() {
    if (instagramTimer) {
        clearInterval(instagramTimer);
        instagramTimer = null;
    }
}

router.get('/stop', function(req, res) {
    stopTwitter();
    stopInstagram();
    res.send("stopped");
});



module.exports = router;