var mysql = require('mysql');
var database_pool = null;

module.exports = function() {

    return {
        init: function(callback) {
            database_pool = mysql.createPool({
                user     : 'adminQzhziuG',
                password : 't2k1srae78yH',
                host: 'http://happilyeverapperley-socialfeed.rhcloud.com',
                port: 3306,
                connectionLimit : 100,
                multipleStatements: true
            });
            database_pool.getConnection(function(err, connection) {
                if (connection) {
                    var query =
                        "CREATE DATABASE IF NOT EXISTS happilyeverapperley;" +
                        "CREATE TABLE IF NOT EXISTS happilyeverapperley.Social(" +
                        "`id` VARCHAR(50) NOT NULL," +
                        "`text` VARCHAR(255)," +
                        "`type` ENUM('twitter', 'instagram')," +
                        "`name` VARCHAR(100)," +
                        "`screen_name` VARCHAR(100)," +
                        "`hashtags` TEXT," +
                        "`media` TEXT," +
                        "`timestamp` INT(11)," +
                        "PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB;";
                    connection.query(query, function(err, result) {
                        if (err) {
                            console.log(err);
                        }
                        connection.release();
                        callback();
                    });
                } else {
                    callback();
                }

            });
        },
        pool: function() {
            return database_pool;
        }
    }

};